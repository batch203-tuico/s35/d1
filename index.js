const { application } = require("express");

// Mongoose is a package that allows us to creat Schemas to model our data structures and to manipulate our database using different access methods.
const mongoose = require("mongoose");
const express = require("express");
const port = 3001;
const app = express();

// [SECTION] MongoDB Connection
/* 
    Syntax:
        mongoose.connect("<MongoDB Atlas Connection String>",
            {
                // Allow us to avoid any current and future errors while connecting to mongoDB

                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        );
*/

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.7jqbank.mongodb.net/batch203_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

    let db = mongoose.connection;

    // Captures and give details of the error when connecting to mongoose
    db.on("error", console.error.bind(console, "connection error"));

    // Successful connection and can made queries
    db.once("open", () => console.log("We're connected to the cloud database."));


// [SECTION] Mongoose Schemas
    // Schemas determine the structure of the documents to be written in the database
    // Schemas act as blueprints to our data
    /* 
        Syntax:
            const schemaName = new mongoose.Schema({<>keyvalue:pair})
    */

    // name & status
    // "required" is used to specify that a field must not be empty.
    // "default" is used if a field value is not supplied.
    const taskSchema = new mongoose.Schema({
        name: {
            type: String,
            required: [true, "Task name is required."]
        },
        status: {
            type: String,
            default: "pending"
        }
    });



// [SECTION] Models
    // Uses schema and use it to create/instantiate documents/object that follows our schema structure.
    // The variable/object what will be created can be used to run commands for interacting with our databse.
    /* 
        // MVC (Model View Controller) Approach (capital first letter of the variable name)
        Syntax:
            const VariableName = mongoose.model("collectionName", schemaName);
    */

    const Task = mongoose.model("Task", taskSchema);

    


// middlewares
app.use(express.json()); // Allow app to read json data

// Allow app to read different data (images etc.) from forms [aside from json data]
app.use(express.urlencoded({extended:true}));



// Business Logic
/*
    1. Add a functionality to check if there are duplicate tasks
        - If the task already exists in the database, we return an error message "Duplicate task found"
        - If the task doesn't exist in the database, we add it in the database
        A task exists if:
            - result from the query is not null
            - result.name is equal to req.body.name
    2. The task data will be coming from the request's body
    3. Create a new Task object with a "name" field/property
    4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

    app.post("/tasks", (req, res) => {
        console.log(req.body.name);
        // "findOne" is a Mongoose method that acts similar to "find" of MongoDB
        // findOne() returns the first document that matches the search criteria
			// If there are no matches, the value of result is null
			// "err" is a shorthand naming convention for errors.
        Task.findOne({name: req.body.name}, (err, result) => {
            console.log(result);
            if (result !== null && result.name == req.body.name){
                return res.send("Duplicate task found!");
            }
            else {
                // "newTask" was created/instantiated from the Mongoose schema and will gain access to ".save" method
                let newTask = new Task({
                    name: req.body.name
                });
                
                newTask.save((saveErr, savedTask) => {
                    if (saveErr){
                        return console.error(saveErr);
                    }
                    else {
                        return res.status(201).send("New task created!");
                    }
                })
            }
        })
    });

    /* 
        // Getting all the tasks

        // Business Logic
    /*
        1. Retrieve all the documents
        2. If an error is encountered, print the error
        3. If no errors are found, send a success status back to the client/Postman and return an array of documents
    */


    app.get("/tasks", (req, res) => {
        Task.find({}, (err, result) => {
            if (err) {
                return console.log(err);
            }
            else {
                return res.status(200).send({
                    data: result
                })
            }
        })
    });

// ======================================================
// ACTIVITY

/* 
    1. Create a User schema.
		username - string
		password - string

*/
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Username is required."]
    },
    password: {
        type: String,
        required: [true, "Password is required."]
    }
});


// 2. Create a User model.
const User = mongoose.model("User", userSchema);


/* 
    Instructions s35 Activity:
        1. Using User model.
        2. Create a POST route that will access the "/signup" route that will create a user.
        3. Process a POST request at the "/signup" route using postman to register a user.

*/

// CREATE new user
app.post("/signup", (req, res) => {
    User.findOne({username: req.body.username}, (err, result) => {
        if (result !== null && result.username == req.body.username){
            return res.send("Existing username!")
        }
        else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });
            newUser.save((saveErr, savedUser) => {
                if (saveErr){
                    return console.error(saveErr);
                }
                else {
                    return res.status(201).send("New user registered!");
                }
            })
        }
    })
});


// GET all users (for checking)
app.get("/signup", (req, res) => {
    User.find({}, (err, result) => {
        if (err) {
            return console.log(err);
        }
        else {
            return res.status(200).send({
                users: result
            })
        }
    })
});

// ==================================================================


app.listen(port, () => console.log(`Server is running at port ${port}`));